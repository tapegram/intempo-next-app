import Link from "next/link";
import { useTournamentsQuery } from "../../lib/tournaments.graphql";

const Index = () => {
  const { tournaments } = useTournamentsQuery().data!;

  return (
    <div>
      {tournaments.map((t) => (
        <li key={t!!.id}>
          <Link href={`/tournament/${t!!.id}`}>
            <a>{t!!.name}</a>
          </Link>
        </li>
      ))}
    </div>
  );
};

export default Index;
