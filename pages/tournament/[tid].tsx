import { useTournamentQuery } from "../../lib/tournament.graphql";
import { useRouter } from "next/router";

const Tournament = () => {
  const router = useRouter();
  const { tid } = router.query;

  const { data } = useTournamentQuery({
    variables: {
      id: tid as string,
    },
  });
  return <p>{data?.tournament?.name}</p>;
};

export default Tournament;
