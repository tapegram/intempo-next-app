import Link from "next/link";
import {
  useTournamentsQuery,
  TournamentsDocument,
} from "../lib/tournaments.graphql";
import { initializeApollo } from "../lib/apollo";

const Index = () => {
  const { tournaments } = useTournamentsQuery().data!;

  return (
    <div>
      {tournaments.map((t) => (
        <li key={t!!.id}>
          <Link href={`/tournament/${t!!.id}`}>
            <a>{t!!.name}</a>
          </Link>
        </li>
      ))}
    </div>
  );
};

export async function getStaticProps() {
  const apolloClient = initializeApollo();

  await apolloClient.query({
    query: TournamentsDocument,
  });

  return {
    props: {
      initialApolloState: apolloClient.cache.extract(),
    },
  };
}

export default Index;
