export type Dependencies = {
  DB: DB;
};
export type DB = {
  fetchTournament: (id: string) => Tournament | null;
  fetchTournaments: () => Tournament[];
  createTournament: (tournament: Tournament) => null;
};

export type TournamentName = string;
export type Tournament = {
  id: string;
  name: TournamentName;
};

export const toTournament = (id: string, name: TournamentName): Tournament => {
  return {
    id,
    name,
  };
};

export const fetchTournaments = (dependencies: Dependencies) =>
  dependencies.DB.fetchTournaments();
export const fetchTournament = (dependencies: Dependencies, id: string) =>
  dependencies.DB.fetchTournament(id);
