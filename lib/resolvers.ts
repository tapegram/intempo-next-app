import { QueryResolvers } from "./schema.graphqls";
import { ResolverContext } from "./apollo";
import {
  fetchTournaments,
  fetchTournament,
  Dependencies,
  toTournament,
  Tournament,
} from "../service/app";

const hardCodedDependencies: Dependencies = {
  DB: {
    fetchTournaments: () => [
      toTournament("1", "NAC D"),
      toTournament("2", "NAC E"),
    ],
    fetchTournament: (id: string) => toTournament(id || "999", "NAC D"),
    createTournament: (tournament: Tournament) => null,
  },
};

const Query: Required<QueryResolvers<ResolverContext>> = {
  tournaments: (_parent, _args, _context, _info) =>
    fetchTournaments(hardCodedDependencies),

  tournament: (_parent, _args, _context, _info) => {
    return fetchTournament(hardCodedDependencies, _args.id!!);
  },
};

export default { Query };
